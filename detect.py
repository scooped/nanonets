import time
import tensorflow as tf
import numpy as np
import math
import cv2


def make_divisible(x, divisor):
    # Returns x evenly divisible by divisor
    return math.ceil(x / divisor) * divisor

def check_img_size(imgsz, s=32, floor=0):
    # Verify image size is a multiple of stride s in each dimension
    if isinstance(imgsz, int):  # integer i.e. img_size=640
        new_size = max(make_divisible(imgsz, int(s)), floor)
    else:  # list i.e. img_size=[640, 480]
        new_size = [max(make_divisible(x, int(s)), floor) for x in imgsz]
    if new_size != imgsz:
        print(f'WARNING: --img-size {imgsz} must be multiple of max stride {s}, updating to {new_size}')
    return new_size


def xywh2xyxy(x):
    # Convert nx4 boxes from [x, y, w, h] to [x1, y1, x2, y2] where xy1=top-left, xy2=bottom-right
    y = 1 * x if isinstance(x, tf.Tensor) else np.copy(x)
    y0 = x[:, 0] - x[:, 2] / 2  # top left x
    y1 = x[:, 1] - x[:, 3] / 2  # top left y
    y2 = x[:, 0] + x[:, 2] / 2  # bottom right x
    y3 = x[:, 1] + x[:, 3] / 2  # bottom right y
    return tf.transpose(tf.stack([y0, y1, y2, y3]))


def box_iou(box1, box2):
    # https://github.com/pytorch/vision/blob/master/torchvision/ops/boxes.py
    """
    Return intersection-over-union (Jaccard index) of boxes.
    Both sets of boxes are expected to be in (x1, y1, x2, y2) format.
    Arguments:
        box1 (Tensor[N, 4])
        box2 (Tensor[M, 4])
    Returns:
        iou (Tensor[N, M]): the NxM matrix containing the pairwise
            IoU values for every element in boxes1 and boxes2
    """

    def box_area(box):
        # box = 4xn
        return (box[2] - box[0]) * (box[3] - box[1])

    area1 = box_area(box1.T)
    area2 = box_area(box2.T)

    # inter(N,M) = (rb(N,M,2) - lt(N,M,2)).clamp(0).prod(2)
    inter = (tf.math.minimum(box1[:, None, 2:], box2[:, 2:]) - tf.math.maximum(box1[:, None, :2], box2[:, :2])).clamp(0).prod(2)
    return inter / (area1[:, None] + area2 - inter)  # iou = inter / (area1 + area2 - inter)


def non_max_suppression(prediction, conf_thres=0.25, iou_thres=0.45, classes=None, agnostic=False, multi_label=False,
                        labels=(), max_det=300):
    """Runs Non-Maximum Suppression (NMS) on inference results

    Returns:
         list of detections, on (n,6) tensor per image [xyxy, conf, cls]
    """

    nc = prediction.shape[2] - 5  # number of classes
    xc = prediction[..., 4] > conf_thres  # candidates
    # print(prediction.shape)
    # print(prediction[0,0:])

    # Checks
    assert 0 <= conf_thres <= 1, f'Invalid Confidence threshold {conf_thres}, valid values are between 0.0 and 1.0'
    assert 0 <= iou_thres <= 1, f'Invalid IoU {iou_thres}, valid values are between 0.0 and 1.0'

    # Settings
    min_wh, max_wh = 2, 4096  # (pixels) minimum and maximum box width and height
    max_nms = 30000  # maximum number of boxes into tfvision.ops.nms()
    time_limit = 10.0  # seconds to quit after
    redundant = True  # require redundant detections
    multi_label &= nc > 1  # multiple labels per box (adds 0.5ms/img)
    merge = False  # use merge-NMS

    t = time.time()
    output = []
    for xi, x in enumerate(prediction):  # image index, image inference
        # Apply constraints
        # x[((x[..., 2:4] < min_wh) | (x[..., 2:4] > max_wh)).any(1), 4] = 0  # width-height
        x = x[xc[xi]]  # confidence

        # Cat apriori labels if autolabelling
        if labels and len(labels[xi]):
            l = labels[xi]
            v = tf.zeros((len(l), nc + 5))
            v[:, :4] = l[:, 1:5]  # box
            v[:, 4] = 1.0  # conf
            v[range(len(l)), l[:, 0].long() + 5] = 1.0  # cls
            x = tf.concat((x, v), 0)
            print("Why do we suffer")

        # If none remain process next image
        if not x.shape[0]:
            continue


        # print(x[0,:])
        # Compute conf
        conf = tf.expand_dims(x[:, 4], axis=1)
        conf = tf.tile(conf, [1, x.shape[1] - 5])
        y = x[:, 5:] * conf  # conf = obj_conf * cls_conf
        z = x[:, :5]

        x = tf.concat([z, y], axis=1)

        # Box (center x, center y, width, height) to (x1, y1, x2, y2)
        box = xywh2xyxy(x[:, :4])
        # print(box.shape)
        # print(box)
        # print('that was the shape of the box')

        # Detections matrix nx6 (xyxy, conf, cls)
        if multi_label:
            i, j = (x[:, 5:] > conf_thres).nonzero(as_tuple=False).T
            x = tf.concat((box[i], x[i, j + 5, None], j[:, None].float()), 1)
        else:  # best class only

            j = tf.math.argmax(x[:, 5:], axis=1)
            j = tf.cast(j, tf.dtypes.float32)
            j = tf.expand_dims(j, axis=1)

            conf = tf.math.reduce_max(x[:, 5:], axis=1, keepdims=True)
            x = tf.concat((box, conf, j), axis=1)[tf.reshape(conf, -1) > conf_thres]

        # Filter by class
        # if classes is not None:
        #     x = x[(x[:, 5:6] == tf.tensor(classes, device=x.device)).any(1)]

        # Apply finite constraint
        # if not tf.isfinite(x).all():
        #     x = x[tf.isfinite(x).all(1)]

        # Check shape
        n = x.shape[0]  # number of boxes
        if not n:  # no boxes
            continue
        elif n > max_nms:  # excess boxes
            x = x[x[:, 4].argsort(descending=True)[:max_nms]]  # sort by confidence

        # Batched NMS
        c = x[:, 5] * (0 if agnostic else max_wh)  # classes
        c = tf.expand_dims(c, axis=1)
        c = tf.tile(c, [1,4])
        boxes, scores = x[:, :4] + c, x[:, 4]  # boxes (offset by class), scores
        i = tf.image.non_max_suppression(boxes, scores, max_det, iou_threshold= iou_thres)  # NMS
        if i.shape[0] > max_det:  # limit detections
            i = i[:max_det]
        if merge and (1 < n < 3E3):  # Merge NMS (boxes merged using weighted mean)
            # update boxes as boxes(i,4) = weights(i,n) * boxes(n,4)
            iou = box_iou(boxes[i], boxes) > iou_thres  # iou matrix
            weights = iou * scores[None]  # box weights
            x[i, :4] = tf.matmul(weights, x[:, :4]).float() / weights.sum(1, keepdim=True)  # merged boxes
            if redundant:
                i = i[iou.sum(1) > 1]  # require redundancy

        values = []
        for index in i.numpy():
            values.append(x[index])

        output.append(tf.Variable(values))
        if (time.time() - t) > time_limit:
            print(f'WARNING: NMS time limit {time_limit}s exceeded')
            break  # time limit exceeded

    return output



def read_img(path):
    img = tf.io.read_file(path)
    img = tf.image.decode_jpeg(img, channels=3)
    img = tf.image.convert_image_dtype(img, tf.float32)
    return img


conf_thres=0.25 # confidence threshold
iou_thres=0.45 # NMS IOU threshold
max_det=20
stride = 64
imgsz = [640,640]

model = tf.keras.models.load_model('./yolov5s_saved_model')
im_name = 'test2.jpg'
im = read_img(im_name)
tf_var = tf.Variable([im])
imgsz = check_img_size(imgsz, s=stride)
pred = model(tf_var)

temp = [imgsz[1], imgsz[0], imgsz[1], imgsz[0]]
temp = tf.concat([temp, tf.ones(81)], axis=0)
temp = tf.expand_dims(temp, axis=0)
temp = tf.expand_dims(temp, axis=0)
temp = tf.tile(temp, [1, 25200, 1])

pred = pred * temp

img = cv2.imread(im_name)
# pred[..., 0] *= imgsz[1]  # x
# pred[..., 1] *= imgsz[0]  # y
# pred[..., 2] *= imgsz[1]  # w
# pred[..., 3] *= imgsz[0]  # h
pred = non_max_suppression(pred, conf_thres, iou_thres, None, False, max_det=max_det)
for i, det in enumerate(pred):
    # Print results
    print(det)
    for d in det.numpy():
        x = int(d[0])
        y = int(d[1])
        w = int(d[2])
        h = int(d[3])
        p1 = (x, y)
        p2 = (w, h)
        img = cv2.rectangle(img, p1, p2, (255, 0, 0), 2)
cv2.imshow("test", img)
cv2.waitKey(0)

