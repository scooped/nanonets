import tensorflow as tf
from PIL import Image
import numpy as np
import time


def read_img(path):
    img = tf.io.read_file(path)
    img = tf.image.decode_jpeg(img, channels=3)
    img = tf.image.convert_image_dtype(img, tf.float32)
    return img

model = tf.keras.models.load_model('./yolov5s_saved_model')

im = read_img('test.jpg')
tf_var = tf.Variable([im])
print("start")
pred = model(tf_var)
print("end")

start = time.time()
for x in range(100):
    pred = model(tf_var)
end = time.time()
print(1 / ((end - start)/800))
